#!/usr/bin/env python
from utils.database.database import Database
from utils.cursor_parser import *
import config.mongoconfig as cfg_mongo
import config.selogerconfig as cfg_sl


def main():
    d = Database(cfg_mongo.mongo['host'], cfg_mongo.mongo['port'], cfg_sl.mongo['database'], cfg_sl.mongo['collection'])
    d.get_client()
    d.connect()

    chunk_iter = d.iterate_by_chunks(200, 0, query={'detail_scraped': True, 'inseeCode': {'$exists': False}})

    CursorParser(chunk_iter, cfg_sl.thread['name'], d).parse()


if __name__ == '__main__' :
    main()
