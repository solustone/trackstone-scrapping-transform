# Project Goals
This project is where we store the transform logic for differents sources  
to make them 'Tracktone' usable.

# Requirements :
  - Python 3
  - Pip
  - Virutalenv
# Setup :
## Virutalenv setup :  
### Install :
`virtualenv venv`  

### Activate :
This may differ depending on the termial :
  - For FISH :
  `. venv/bin/activate.fish`
  - For Bash :
  `source venv/bin/activate`

### Dependency installation :
To manage dependecies, pip is used.  
To install the dependecies use :
`pip install -r requirements.txt`

### Update Dependencies :
`pip freeze > requirements.txt`



### Conf 
In the conf folder, you will find differents settings. Edit as needed.
Might add to .gitinore ? 


# RUN 
`./seloger-transform.py`
# TODO
Docker
Expand
Mutualise code
