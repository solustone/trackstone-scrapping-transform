import importlib
from utils import timing


class CursorParser:
    
    def __init__(self, cursor, source, database):
        self.cursor = cursor
        self.source = source
        self.database = database

    @timing
    def parse(self):
        threads = []
        for docs in self.cursor:
            localthread = getattr(importlib.import_module("utils.threads.sources."+self.source), self.source)
            t = localthread(docs, self.database)
            t.start()
            threads.append(t)
        for t in threads :
            t.join()

        print('DONE')