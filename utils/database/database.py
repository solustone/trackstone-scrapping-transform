from pymongo import MongoClient

class Database(object):
    """
     Database connecter Absctrac
    """

    def __init__(self, host, port, database, collection):
        self.host = host
        self.port = port
        self.database = database
        self.collection = collection

    def get_client(self):
        try:
            self.client = MongoClient(self.host, self.port)
            return self.client
        except:
            print('Failed to create MongoClient')
            raise

    def connect(self):
        try :
            self.db = self.client[self.database]
            return self.db
        except :
            print('Failed to connect to database')
            raise

    def get_collection(self):
        try:
            return self.db[self.collection]
        except:
            raise

    def full_data_cursor(self):
        try :  
            return self.db[self.collection].find().batch_size(100)
        except :
            print('failed to retrieve mongo collection cursor')
            raise

    def iterate_by_chunks(self, chunksize=1, start_from=0, query={}):
        chunks = range(start_from, self.db[self.collection].find(query).count(), int(chunksize))
        num_chunks = len(chunks)
        for i in range(1,num_chunks+1):
            if i < num_chunks:
                yield self.db[self.collection].find(query)[chunks[i-1]:chunks[i]]
            else:
                yield self.db[self.collection].find(query)[chunks[i-1]:chunks.stop]