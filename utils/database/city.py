import mysql.connector
import config.sqlconfig as sql_cfg


def connect():
    try:
        return mysql.connector.connect(**sql_cfg.conn)
    except:
        print('Failed to connect to City database')
        raise


def to_dict():
    conn = connect()
    curs = conn.cursor()
    curs.execute('SELECT name, postal_code, insee_code FROM city')
    res = curs.fetchall()

    dct = { a: (a, b, c) for a,b,c in res}
    return dct


cities = to_dict()