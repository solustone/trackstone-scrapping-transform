import unicodedata
from re import sub

# Transforme au format dataGouv
def city_trim(city): 
    city = city.upper().replace('-',' ')
    city = sub('(^| )(SAINT) ', 'ST ', city)
    city = ''.join((c for c in unicodedata.normalize('NFD', city) if unicodedata.category(c) != 'Mn')).replace('\'', ' ')
    return city