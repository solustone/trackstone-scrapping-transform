from threading import Thread
from utils.database.city import cities
from utils.string import city_trim


class seloger(Thread):
    def __init__(self, prop_list, database):
        Thread.__init__(self)
        self.prop_list = prop_list
        self.database = database

    def run(self):
        collection = self.database.get_collection()
        try :
            cpt = 0
            for prop in self.prop_list:
                cpt = cpt+1
                city = city_trim(prop['city'])
                normalize_city = cities.get(city)

                if normalize_city is None:
                    possible_cities = [item for item in cities if city in item]
                    if len(possible_cities) == 1:
                        normalize_city = possible_cities[0]
                    if len(possible_cities) > 1:
                        possible_cities = [item for item in possible_cities if cities.get(item)[1] == prop['zip_code']]
                        if len(possible_cities) == 1:
                            normalize_city = possible_cities[0]
                    if normalize_city is None:
                        for test in cities.items():
                            if int(test[1][1]) == int(prop['zip_code']):
                                normalize_city = test[1]

                if normalize_city is not None:
                    prop['city'] = normalize_city[0]
                    prop['zip_code'] = normalize_city[1]
                    prop['inseeCode'] = normalize_city[2]
                    collection.update_one({"_id": prop["_id"]}, {"$set": {"city": prop['city'], 'inseeCode': prop['inseeCode']}})
        except :
            print('Failed to connect to database')
            raise